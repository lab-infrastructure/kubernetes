# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/null" {
  version     = "3.2.3"
  constraints = ">= 3.2.2"
  hashes = [
    "h1:+AnORRgFbRO6qqcfaQyeX80W0eX3VmjadjnUFUJTiXo=",
    "zh:22d062e5278d872fe7aed834f5577ba0a5afe34a3bdac2b81f828d8d3e6706d2",
    "zh:23dead00493ad863729495dc212fd6c29b8293e707b055ce5ba21ee453ce552d",
    "zh:28299accf21763ca1ca144d8f660688d7c2ad0b105b7202554ca60b02a3856d3",
    "zh:55c9e8a9ac25a7652df8c51a8a9a422bd67d784061b1de2dc9fe6c3cb4e77f2f",
    "zh:756586535d11698a216291c06b9ed8a5cc6a4ec43eee1ee09ecd5c6a9e297ac1",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:9d5eea62fdb587eeb96a8c4d782459f4e6b73baeece4d04b4a40e44faaee9301",
    "zh:a6355f596a3fb8fc85c2fb054ab14e722991533f87f928e7169a486462c74670",
    "zh:b5a65a789cff4ada58a5baffc76cb9767dc26ec6b45c00d2ec8b1b027f6db4ed",
    "zh:db5ab669cf11d0e9f81dc380a6fdfcac437aea3d69109c7aef1a5426639d2d65",
    "zh:de655d251c470197bcbb5ac45d289595295acb8f829f6c781d4a75c8c8b7c7dd",
    "zh:f5c68199f2e6076bce92a12230434782bf768103a427e9bb9abee99b116af7b5",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.6.3"
  constraints = ">= 3.6.2"
  hashes = [
    "h1:Fnaec9vA8sZ8BXVlN3Xn9Jz3zghSETIKg7ch8oXhxno=",
    "zh:04ceb65210251339f07cd4611885d242cd4d0c7306e86dda9785396807c00451",
    "zh:448f56199f3e99ff75d5c0afacae867ee795e4dfda6cb5f8e3b2a72ec3583dd8",
    "zh:4b4c11ccfba7319e901df2dac836b1ae8f12185e37249e8d870ee10bb87a13fe",
    "zh:4fa45c44c0de582c2edb8a2e054f55124520c16a39b2dfc0355929063b6395b1",
    "zh:588508280501a06259e023b0695f6a18149a3816d259655c424d068982cbdd36",
    "zh:737c4d99a87d2a4d1ac0a54a73d2cb62974ccb2edbd234f333abd079a32ebc9e",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:a357ab512e5ebc6d1fda1382503109766e21bbfdfaa9ccda43d313c122069b30",
    "zh:c51bfb15e7d52cc1a2eaec2a903ac2aff15d162c172b1b4c17675190e8147615",
    "zh:e0951ee6fa9df90433728b96381fb867e3db98f66f735e0c3e24f8f16903f0ad",
    "zh:e3cdcb4e73740621dabd82ee6a37d6cfce7fee2a03d8074df65086760f5cf556",
    "zh:eff58323099f1bd9a0bec7cb04f717e7f1b2774c7d612bf7581797e1622613a0",
  ]
}

provider "registry.terraform.io/invidian/sshcommand" {
  version     = "0.2.2"
  constraints = ">= 0.2.2"
  hashes = [
    "h1:hCfqgTFwuiXLT422LvDC3E4S9bylhQpM4nnxiBQZ+60=",
    "zh:365c8f46d4a895088b0d157487cd606d5274a25575f411eebeb0aa9dc3fc5471",
    "zh:48bd318ea25248cb01adc83a1f75c485addad5fcbc63344f2568604b5b0d5484",
    "zh:4b67ecd0e34f7d6a9a21bb384d57a4f7fbdba0a393d45ba3de5bc74bdf0d0d70",
    "zh:5f509301447a9ce55716f7d327883d7ce6d0533fd1eb10e8a4863b596155d1d8",
    "zh:6a3275345a995448b90baa5c582e26e23ab88804c3b738798cbae908048a3701",
    "zh:8a34029dd7879711cddcea42bc534f585700ccb977a95cfe804555ec9c046ba0",
    "zh:952e7bf7a23fe9e5b4131e6e5661fe6a330e35c06ca386f7bc7a16f5c12400c7",
    "zh:bf3c6f2bbf9518655cd719e40fbe7708d379e89519e8619248394828721b6d4f",
    "zh:ce735eab2684b3a91a97106d6d8a635daaa3a52a74d3f6352541eb4e201fc4c1",
    "zh:cef6d47a305e679353353a305fc96f7d3f7e70e7feca8f000f69ed8a48e366b8",
    "zh:ff9e175747fa7c3fcd3e009cf1ae40b14b8786a92e362d4426a92c5b6aca9c40",
  ]
}

provider "registry.terraform.io/ivoronin/macaddress" {
  version     = "0.3.2"
  constraints = ">= 0.3.0"
  hashes = [
    "h1:yk0ASl2cAoc/22tvpi9Kke+WvowgXGq0QwaP93IQ+S0=",
    "zh:00cb168d9210ed88cfa7de8a33d5666b2cf6660a5d20a7a96348b8b902833eca",
    "zh:1366458320df0b6f1132e59b5410931c0c5626bbf27b05b29dd311311a710e9b",
    "zh:2e8102c7f6046665c95b806752d692843f2e846554f7eba85690cd2087c9048a",
    "zh:3c1ae52f855d0e694ad28eb34ec41c553344aaa7bd51adaa48cf15e3ee842e17",
    "zh:496d8db2055cead9d264fdad83534318e3ab77ce06e38d43674a4ec25c0e860d",
    "zh:54c5eeae7cc61d706080256e06aaf509869b1d86297b9e99948a2fe2af6d455b",
    "zh:5f26e851048be3c56f3706b7fde25fe76dd30003ef6356216dc9ecff400218bb",
    "zh:5fc1debcd0fe043dfce00ab110e180b896a1a9958edea7d81d05aacc9b630e5e",
    "zh:650045261b382b4559fd1bd190d6cabbeb022b53d7e240eb6b66f6824ca81bf4",
    "zh:7203dea017883e8fdd7ba66c9b1a9aac0cab101133e4eeab365c4d0995194272",
    "zh:726a9222d15f11316587c199ee367bae1d5495ff16ebdfc41635f7628834a8d6",
    "zh:c9f3bcaa073a0921189bd74ef6b2b57cad34b3eb01788c010df8a15fd9d8045c",
    "zh:d3fba491b0ff0d3d64162216159232398a75ad81c31e4304335d6b76b74a864a",
    "zh:e80011c6e3af4eeafdeda9bd118a774f8b7cdf1f133953abf827f313653ec184",
  ]
}

provider "registry.terraform.io/terra-farm/xenorchestra" {
  version     = "0.26.1"
  constraints = "0.26.1"
  hashes = [
    "h1:2XM3BMCV8OzPXzZIZszHDGbfzQdczv8JiYjHSRhwOCA=",
    "zh:207dd00fe6e43d74eada928dccf5d5fa02bd7770275a6fb0133f007af1276cd4",
    "zh:28782a454797ab3da4a2e15fe47b07c0f12e8aa2be6757adcc027115bf94ba8d",
    "zh:5a815f8716c31cf9567066d4f7db54fb53ee8da619844e5266f9043c98bb06d5",
    "zh:6a25a9f8f7636442ddfaa5094ea49974c53b06520a21e973b7a354730f2fbef1",
    "zh:d1c68dc9fb7f5f8186002dd98f424edf11b779cf90225f051a492ff419a95cd0",
    "zh:ebb333315a76b59dbaa79c2aa7fd1e107a7a21a325b48b9c0fe82680d9035732",
    "zh:f0cd326d7a48ab49a1d961f767938925f7af97a0f40c27096d39d3a9964669d9",
  ]
}

provider "registry.terraform.io/vatesfr/xenorchestra" {
  version     = "0.29.0"
  constraints = "0.29.0"
  hashes = [
    "h1:KUQOLHqXwLAYHqtyOzvfpwu4BWSEGnGl5F7isi8jr7M=",
    "zh:097b5e2b779d15045551868b3a3407e16caa60c431476d2ac8b9c7fb3806fe78",
    "zh:11e73bf9a98832d604ea9aaf7ed6ccf82c9969bff4c2ced5ab8035d15c1a2ddd",
    "zh:33a0de184df4dce9450d5bbfa3c7a2eb93605de27d1d6927f192d9f7319efb34",
    "zh:6bbd066bc464585b6bc0c26223d2d0dbd1e26ff637df72ed3105b0fb2c62fb19",
    "zh:aabce1d037b505b15e09bb9235bd0379419237e2129094411c9ffeeb42c365cf",
    "zh:da3ce66b13d2226bc230ea1c659872908d568b21a07b63a17b774ab5ae871dee",
    "zh:fc6aa03bd54d59aa057e724ca061871d64293f333d5c58f127b1e0304c8b7ec0",
  ]
}
