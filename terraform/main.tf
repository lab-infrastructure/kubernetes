terraform {  
  backend "s3" {    
    bucket = "dylan-tf-state"
    key    = "state"
    region = "us-west-2"  
  }
}

data "xenorchestra_template" "ubuntu2004" {
    name_label = "ubuntu-focal-20.04-cloudimg-20231207"
}

data "xenorchestra_template" "ubuntu2004_20241112" {
    name_label = "ubuntu-focal-20.04-cloudimg-20241112"
}

data "xenorchestra_template" "ubuntu2204_202312072" {
    name_label = "ubuntu-jammy-22.04-cloudimg-20231207"
}

data "xenorchestra_template" "ubuntu2404_20241206" {
    name_label = "ubuntu-noble-24.04-cloudimg-20241206"
}




data "xenorchestra_sr" "NFS_VHD_SR" {
  name_label = "NFS-VHD-SR"
}

module "k8s_blue" {
  source = "github.com/InputObject2/terraform-xo-microk8s"
  #version = "0.0.9"

  # Node settings
  node_count               = 3
  node_prefix              = "k8s"
  node_cpu_count           = 8
  node_memory_gb           = 12
  node_os_disk_size        = 120
  node_os_disk_xoa_sr_uuid = [data.xenorchestra_sr.NFS_VHD_SR.id]
  node_xoa_template_uuid   = data.xenorchestra_template.ubuntu2004_20241112.id

  # Master settings
  master_count               = 3
  master_prefix              = "k8s"
  master_cpu_count           = 8
  master_memory_gb           = 6
  master_os_disk_size        = 120
  master_os_disk_xoa_sr_uuid = [data.xenorchestra_sr.NFS_VHD_SR.id]
  master_xoa_template_uuid   = data.xenorchestra_template.ubuntu2004_20241112.id

  # Xen Orchestra settings (can be set via environment variables)
  #xoa_api_url           = "ws://10.0.10.14"
  xoa_pool_name         = "xcp-ng-cofytbnc"
  xoa_network_name      = "k8s.dylanlab.xyz"
  start_delay           = 0

  # Other settings
  cluster_dns_zone = "dylanlab.xyz."

  public_ssh_key   = var.ssh_key_public
  private_ssh_key_path = var.ssh_key_private_path

  dns_zone         = "dylanlab.xyz."
  dns_sub_zone     = "k8s"
  cluster_name     = "blue"

  #vm_timeouts_create = "20m"

  master_expected_cidr = "10.40.0.1/16"
  node_expected_cidr = "10.40.0.1/16"

}
