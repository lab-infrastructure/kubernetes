# Kubernetes Cluster Deployment

Deploys a Kubernetes cluster on XCP-NG using Terraform, Ansible, and RKE using a single command.

## Requirements

This readme assumes you have a baseline configuration as defined here.

* DNS Server with Update Keys
* XCP-NG Hypervisor w/Xen Orchestra Appliance
* Terraform
* Ansible
* RKE


## Setup

1. Copy the example terraform.tfvars file in ``files/examples/`` to the ``terraform/`` folder.
    
    * [Terraform Docs](https://www.terraform.io/docs/language/values/variables.html#variable-definition-precedence)
    * [RKE Terraform Provider Docs](https://registry.terraform.io/providers/rancher/rke/latest/docs)
    * [RKE YAML Docs](https://rancher.com/docs/rke/latest/en/example-yamls/)

2. Configure all variables in ``terraform.tfvars`` as appropriate.


## Usage

1. Terraform Init
    ```sh
    terraform init
    ```

2. Terraform Plan
    ```sh
    terraform plan
    ```

3. If everything looks good, Apply
    ```sh
    terraform apply
    ```
4. The virtual machines should be deployed, ansible playbook will run, then RKE will configure the cluster. You will receive an output with the hostnames, k8s API url, and kube admin user. RKE will save the kube_config_cluter.yaml file in ``files/generated/rke`` 
    ```sh
    Outputs:

    controllers_hostnames = [
    "922fabe1fdab0854.k8s.domain.local",
    "bd1c210d7c910213.k8s.domain.local",
    "67128e192d986139.k8s.domain.local",
    ]
    rke_api_server_url = "https://922fabe1fdab0854.k8s.domain.local:6443"
    rke_kube_admin_user = "kube-admin"
    workers_hostnames = [
    "us20-8b5b4784071cf91d.k8s.domain.local",
    "us20-6deb16ae4a986040.k8s.domain.local",
    "us20-ec7e86f84ff8bd1a.k8s.domain.local",
    "us20-edf9a51de1f077cf.k8s.domain.local",
    "us20-c3efd19b9c636ba6.k8s.domain.local",
    ]
    ```

