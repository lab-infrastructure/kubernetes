

variable "ssh_key_public" {
    type = string
    description = "SSH public key"
}

variable "ssh_key_private_path" {
    type = string
    description = "Path to SSH private key"
    default = "~/.ssh/ansible_id_rsa"
}
